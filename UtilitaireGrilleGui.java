import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class UtilitaireGrilleGui {

	public static Random rand = new Random();

	public static void remplirGuiDepart(int[][] grille, 
			                            GrilleGui gui, 
			                            double nbPourcentage) {

		double nbCase = (gui.getNbLignes()*gui.getNbColonnes()) * nbPourcentage;
		int min = 0, max = 1, i = 0;

		while (i < nbCase)
		{
			int ligne = rand.nextInt(gui.getNbLignes());
			int colonne = rand.nextInt(gui.getNbColonnes());

			if (gui.getValeur(ligne,colonne) == " ")
			{
				gui.setValeur(ligne, colonne, Integer.toString(grille[ligne][colonne]));

				i++;
			}
		}

	}

	public static int getNbValeursValide(int[][] grille, GrilleGui gui) {

		int compteur = 0;
		Color couleurReleve, couleur;
		couleur = Color.white;

		for(int i=0;i<grille.length;i++) {
			for (int j = 0; j < grille[0].length; j++) {

				couleurReleve = gui.getCouleurFond(i,j);

				if (couleur == couleurReleve)
				{
					compteur++;
				}
			}
		}
		System.out.println("Il y a " + compteur + " valeurs valides");
		return 0;
	}

	public static int obtenirErreurs(GrilleGui gui, int [][] grille) {

		String val1, val2, valAct;


		for(int i=0;i<grille.length;i++){
			for(int j=0;j<grille[0].length;j++){

				int valActEnInt, val1EnInt, val2EnInt;

				int jSolo = j;
				int jplus1 = j+1;
				int jplus2 = j+2;

				valAct = gui.getValeur(i,jSolo);
				val1 = gui.getValeur(i,jplus1);
				val2 = gui.getValeur(i,jplus2);

				valActEnInt = valeurGuiConvertitEnInt(valAct);
				val1EnInt = valeurGuiConvertitEnInt(val1);
				val2EnInt = valeurGuiConvertitEnInt(val2);
				if (valAct != " " || val1 != " " || val2 != " ")
				{

					if ( valActEnInt == 1 && val1EnInt == 1 && val2EnInt == 1 )
					{
						gui.setCouleurFond(i,jSolo,Color.red);
						gui.setCouleurFond(i,jplus1,Color.red);
						gui.setCouleurFond(i,jplus2,Color.red);
					}
					else if ( valActEnInt == 0 && val1EnInt == 0 && val2EnInt == 0 )
					{
						gui.setCouleurFond(i,jSolo,Color.red);
						gui.setCouleurFond(i,jplus1,Color.red);
						gui.setCouleurFond(i,jplus2,Color.red);
					}
				}
			}
		}
		return 0;
	}

	public static boolean grilleEstPleine(GrilleGui gui) {
		int nbLignes, nbColonnes, i, j;
		String valVide;
		int Compteur = 0;
		int valActEnInt, val1EnInt, val2EnInt;

//		valVide = gui.getValeur(i,j);
//		System.out.println("valeur de valVide = " + valVide);
		nbLignes = gui.getNbLignes();
		nbColonnes = gui.getNbColonnes();

		System.out.println("Nombre de lignes = " + nbLignes);
		System.out.println("Nombre de colonnes = " + nbColonnes);

			for(i=0;i<nbLignes;i++){
			for(j=0;j<nbColonnes;j++){

				valVide = gui.getValeur(i,j);
				if (valVide == " " ) {
					Compteur++;
					gui.setCouleurFond(i,j, Color.blue);
				}
				else
					{
						System.out.println("la grille n'est pas pleine!");
					}
			}
		}
			if (Compteur == 0)
			{
				System.out.println("la grille est pleine");
				return true;
			}
			else
			{return false;}

	}

	public static void reinitialiserCouleur(GrilleGui gui) {

			int nbLignes, nbColonnes, i, j;

			nbLignes = gui.getNbLignes();
			nbColonnes = gui.getNbColonnes();

			for (i = 0; i < nbLignes; i++) {
				for (j = 0; j < nbColonnes; j++) {

					gui.setCouleurFond(i, j, Color.white);

				}
			}
	}

	public static void afficherChangementGrilleGui(GrilleGui gui) {
		
		
	}

	public static void ajusterGui(GrilleGui gui) {
		
		
	}

	public static void remettreGrilleDepart(int[][] grille, GrilleGui gui) {



	}

	public static int valeurGuiConvertitEnInt(String valeur) {


		int valGui = Integer.parseInt(valeur);

		return valGui;
	}


}
